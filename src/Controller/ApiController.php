<?php

namespace App\Controller;

use Borsaco\TelegramBotApiBundle\Service\Bot;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(Bot $botService): Response
    {
        $bot = $botService->getBot('soyouz');

        $updates = $bot->getUpdates();

        foreach ($updates as $update) {
            var_dump($update->detectType());
        }

        var_dump($updates);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ApiController.php',
        ]);
    }
}
